package br.com.erudio.services.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import br.com.erudio.models.Person;
import br.com.erudio.services.PersonService;

@Service
public class PersonServiceImpl implements PersonService{

	private final AtomicLong counter = new AtomicLong();
	
	public Person create(Person person) {
		return person;
	}

	public Person findById(String personId) {
		Person person = new Person();
        person.setId(counter.incrementAndGet());
        person.setFirstName("Leandro");
        person.setLastName("Costa");
        person.setAdress("Uberlândia - Minas Gerais - Brasil");
        return person;
	}

	public List<Person> findAll() {
		ArrayList<Person> persons = new ArrayList();
        for (int i = 0; i < 8; i++) {
            Person person = mockPerson(i);
            persons.add(person);
        }
        return persons;
	}

	public Person update(Person person) {
		return person;
	}

	public void delete(String personId) {
		
	}

	// Método responsável por mockar uma pessoa
    private Person mockPerson(int i) {
        Person person = new Person();
        person.setId(counter.incrementAndGet());
        person.setFirstName("Person Name " + i);
        person.setLastName("Last Name " + i);
        person.setAdress("Some Address in Brasil " + i);
        return person;
    }
    
	
}
